package digital.porsche.basicendpoint

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BasicEndpointApplication

fun main(args: Array<String>) {
    runApplication<BasicEndpointApplication>(*args)
}
