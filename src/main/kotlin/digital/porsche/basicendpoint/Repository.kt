package digital.porsche.basicendpoint

import org.springframework.data.jpa.repository.JpaRepository

interface Repository: JpaRepository<Data, String>