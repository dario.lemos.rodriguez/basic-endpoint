package digital.porsche.basicendpoint

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@org.springframework.stereotype.Controller
class Controller(
    @Autowired
    private val repository: Repository
) {
    @GetMapping(params = ["from", "to"], produces = ["application/json"], path = ["/data"])
    fun getData(@RequestParam from: Int, @RequestParam to: Int, @RequestParam name: String): Map<String, Int> {
        var result = 0
        for (i in from .. to) {
            result += i
        }
        val map = mutableMapOf<String, Int>()
        map[name] = result

        repository.save(Data(name, result))

        return map
    }
}