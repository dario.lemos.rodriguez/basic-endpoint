package digital.porsche.basicendpoint

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "data")
class Data (
    @Id
    @Column(nullable = false)
    val name: String,

    @Column(nullable = false)
    val result: Int
)